#!/bin/bash

# Get current datetime for the directory name
date=$(TZ=Asia/Bangkok date +%Y-%m-%dT%H-%M-%S)
echo $date

# Download data from covid19-cdn.workpointnews.com
workpoint_files=("cases" "constants" "trend" "world")
mkdir "current"
for file in ${workpoint_files[*]}; do
	wget "https://covid19-cdn.workpointnews.com/api/$file.json" -O "current/$file.json"
done

# Download data from csse_covid_19_time_series
csse_files=("time_series_covid19_confirmed_global" "time_series_covid19_deaths_global" "time_series_covid19_recovered_global")
mkdir "csse"
for file in ${csse_files[*]}; do
	wget "https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/$file.csv" -O "csse/$file.csv"
done


# Download the previous snapshots on its own gitlab pages
wget "https://covid19.spicydog.org/archive.tar.xz" -O "archive.tar.xz"

echo "extract archive.tar.xz"
tar -xvf archive.tar.xz

# Make data directory just in case
mkdir -p "data"

# Remove all empty directories
find data/ -type d -empty -delete

# Ready to archive the current data
cp -r "current" "data/$date"

# Add workpointnews directory
cp -r "current" "data/workpointnews"

# Move csse to the data, we do not snapshot csse data
rm -rf "data/csse"
cp -r "csse" "data/csse"

# A public directory for GitLab Pages
mkdir "public"

# Archive the data for future use
echo "compress archive.tar.xz"
tar -cf - "data" | xz -9 -c - > "archive.tar.xz"
mv "archive.tar.xz" "public"

# Remove data directory that store workpointnews snapshot data as we no longer use
rm -rf data
mkdir data

# Add csse and workpointnews directory
cp -r "current" "data/workpointnews"
cp -r "csse" "data/csse"

# Publish all the data
mv "data" "public"

# Add static files for the user
cp "icon.png" "public"
cp -r "preview" "public"

# Replace preview image of index.html and copy to public dir
sed "s/preview.png/preview\/$CI_PIPELINE_ID.png/g" "index.html" > "public/index.html"